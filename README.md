# dep-graph

A Leiningen plugin that generates a graphviz file containing the dependency graph of the namespaces in your project.

## Usage

Put `[dep-graph "0.1.0"]` into the `:plugins` vector of your
`:user` profile.

Put `[dep-graph "0.1.0"]` into the `:plugins` vector of your project.clj.

Run:

    $ lein dep-graph

Which will give you a file $PROJECT.dot that you can generate several types of images from.

In order to generate an image from the output file you will need dot installed. See www.graphviz.org on how to obtain that for your system.

An example to generate an svg:

    $ lein dep-graph
    $ dot -Tsvg -O $PROJECT.dot

Or a png:

    $ lein dep-graph
    $ dot -Tpng -O $PROJECT.dot

## License

Copyright © 2014 Trevis Elser

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.