(defproject dep-graph "0.1.1-SNAPSHOT"
  :description "Yet another dependency graph plugin for Leiningen"
  :url "http://gitlab.com/telser/dep-graph"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :eval-in-leiningen true
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.namespace "0.2.4"]])
