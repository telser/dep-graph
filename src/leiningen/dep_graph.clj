(ns leiningen.dep-graph
  (:require [clojure.java.io :as io]
            [clojure.set :as set]
            [clojure.string :as str]
            [clojure.tools.namespace.file :as file]
            [clojure.tools.namespace.track :as track]
            [clojure.tools.namespace.find :as find]
            [clojure.tools.namespace.dependency :as dep]
            [leiningen.core.project :as project]))

(def dot-preamble "digraph simple_hierarchy { \n graph [rankdir = \"LR\"];\n")

(defn dot-str
  "Dot doesn't like dots or dashes so change them to underscores."
  [s]
  (str/replace s #"[.-]" "_"))

(defn write-graph-file
  "Write out dot file."
  [deps edges outfile]
  (let [ks (keys edges)]
    (loop [ks ks ne "" n ""]
      (if (empty? ks)
        (with-open [file (io/writer outfile)]
          (do
            (.write file (str dot-preamble ne
                              "subgraph cluster_clojure {\n label=\"clojure\";\n"
                              "color=blue;\n"))
            (let [singletons (set/difference (set deps) (set (keys edges)))]
              (doseq [s singletons]
                (.write file (dot-str (str s "[label=\"" s "\"];\n")))))
            (.write file (str n "}}"))))
        (let [k (first ks)
              depends (k edges)
              node-edges (reduce str (map #(str (dot-str (name k)) "->"
                                                (dot-str %) ";\n")
                                          depends))
              node (dot-str (str (name k) "[label=\"" (name k) "\"];\n"))]
          (recur (rest ks) (str ne node-edges) (str n node)))))))

(defn graph-with-subs
  "The project has subprojects, include their namespaces/deps"
  [project subs outfile & args]
  (loop [subs subs
         deps #{}
         edges {}]
    (if (empty? subs)
      (write-graph-file deps edges outfile)
      (let [prj (project/init-project
                 (project/read (str (first subs) "/project.clj")))
            sources (apply set/union
                           (map #(find/find-clojure-sources-in-dir (io/file %))
                                (:source-paths prj)))
            tracker (file/add-files {} sources)
            dep-graph (:dependencies (tracker ::track/deps))
            all-deps (set/union deps (reduce set/union (vals dep-graph)))]
        (recur (rest subs) all-deps (merge edges dep-graph))))))

(defn dep-graph
  "Generate a dependency graph of the namespaces required as a dot file"
  [project & args]
  (let [subs (seq (:sub project))]
    (if subs
      (graph-with-subs project subs (str (:name project) ".dot") args)
      (let [sources (apply set/union
                           (map #(find/find-clojure-sources-in-dir (io/file %))
                                (:source-paths project)))
            tracker (file/add-files {} sources)
            dep-graph (:dependencies (tracker ::track/deps))
            total-deps (reduce set/union (vals dep-graph))]
        (write-graph-file total-deps dep-graph (str (:name project) ".dot"))))))
